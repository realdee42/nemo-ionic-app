import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventData {
  data: any;

  constructor(public http: HttpClient) {
  }

  load(): any {
    if (this.data) {
      console.log('data is there');
      return of(this.data);
    } else {
      console.log('data requested from server');
      return this.http
        .get('https://nemo-bb.de/wp-json/wp/v2/events?per_page=50')
        .pipe(map(this.processData, this));
    }
  }

  processData(data: any) {
    this.data = data;
    return this.data;
  }

  loadEvents() {
    return this.load().pipe(
      map((data: any) => {
        console.log('in map');
        console.log(data);
        return data;
      })
    );
  }

  fillEventGroup() {
    const eventGroups = [];
    const startYear = 2016;
    const currentYear = new Date().getFullYear();

    for ( let i = startYear; i <  currentYear + 2; i++ ) {
      eventGroups.push({
        year: i,
        months: []
      });
    }

    eventGroups.forEach( (year) => {
      for ( let m = 0; m < 13; m++ ) {
        year.months.push({
          month: m,
          events: [],
        });
      }
    });

    console.log(eventGroups);

    return eventGroups;
  }

  getEventsByMonth( isReverse = true, segment: string = 'all' ) {
    return this.load().pipe(
      map((data: any) => {

        const currentYear = new Date().getFullYear();
        let eventGroups = this.fillEventGroup();
        const now = new Date();

        data.forEach((event) => {
          // prepare dates
          event.acf.startdate = this.createDateString(event.acf.startdate);
          event.acf.endtime = this.createDateString(event.acf.endtime);

          const eventYear = event.acf.startdate.getFullYear();
          const eventMonth = event.acf.startdate.getMonth();

          if ( this.isInSegment( event, segment ) ) {
            //filter
            if ( event.acf.startdate > now ) {
              eventGroups.forEach( (yearData) => {
                // if same year
                if ( eventYear === yearData.year ) {
                  yearData.months.forEach( monthData => {
                    // if same month
                    if ( eventMonth === monthData.month ) {
                      // push
                      monthData.events.push( event );
                    }
                  });
                }
              });
            }
          }
        });

        eventGroups = this.sortEventsByMonth( eventGroups );

        if ( isReverse ) {
          return eventGroups.reverse();
        } else {
          return eventGroups;
        }
      })
    );
  }

  sortEventsByMonth( data ) {

    const now = new Date();
    data.forEach((yearData) => {
      yearData.months.forEach(monthData => {
        monthData.events.sort( (a, b) => {
          if ( a.acf.startdate > now ) {
            return b.acf.startdate - a.acf.startdate;
          }
        });
      });
    });

    return data;
  }

  createDateString(date) {
    if ( date instanceof Date ) {
      return date;
    }

    let dateString = date.slice(0, 4) + '-' + date.slice(4);
    dateString = dateString.slice(0, 7) + '-' + dateString.slice(7);
    return new Date( dateString );
  }

  isInSegment(event, segment) {

    if ( segment === 'all' ) {
      return true;
    }

    let isInCategory = false;
    event.categories.map( category => {
      // tslint:disable-next-line:radix
      if ( parseInt(segment) === parseInt(category)) {
        isInCategory = true;
      }
    });

    return isInCategory;
  }
}


