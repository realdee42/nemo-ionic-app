import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';
// import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  junior;
  mid;
  senior;
  girls;
  isLoaded = false;

  constructor(
    public storage: Storage,
    public toastController: ToastController
  ) { }

  ngOnInit() {

    this.storage.get('junior').then(res => {
      this.junior = res;
    });
    this.storage.get('mid').then(res => {
      this.mid = res;
    });
    this.storage.get('senior').then(res => {
      this.senior = res;
    });
    this.storage.get('girls').then(res => {
      this.girls = res;
    });

    this.isLoaded = true;
  }

  changeJuniorSettings() {
    this.storage.set( 'junior', this.junior );
    this.toggleTopicSubscription( 'junior', this.junior );
  }
  changeMidSettings() {
    this.storage.set( 'mid', this.mid );
    this.toggleTopicSubscription( 'mid', this.mid );
  }
  changeSeniorSettings() {
    this.storage.set( 'senior', this.senior );
    this.toggleTopicSubscription( 'senior', this.senior );
  }
  changeGirlSettings() {
    this.storage.set( 'girls', this.girls );
    this.toggleTopicSubscription( 'girls', this.girls );
  }

  toggleTopicSubscription( topic, value ) {

    // if( value ) {
    //   FCM.subscribeToTopic( topic ).then( (res) => {
    //     this.presentToast('Nachrichten zum Thema ' + topic + ' erfolgreich abonniert.');
    //   }).catch( reason => {
    //     this.presentToast(reason );
    //   });
    // } else {
    //   FCM.unsubscribeFromTopic( topic ).then( res => {
    //     this.presentToast('Nachrichten zum Thema ' + topic + ' erfolgreich abbestellt.');
    //   }).catch( reason => {
    //     this.presentToast(reason );
    //   });
    // }
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
