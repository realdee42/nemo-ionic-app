import { Component, OnInit } from '@angular/core';
import {EventData} from '../../providers/event-data';
import {UserData} from '../../providers/user-data';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.page.html',
  styleUrls: ['./event-detail.page.scss'],
})
export class EventDetailPage {

  event: any;
  isFavorite = false;
  defaultHref = '';

  constructor(
    private dataProvider: EventData,
    private userProvider: UserData,
    private route: ActivatedRoute
  ) { }

  ionViewWillEnter() {
    // tslint:disable-next-line:radix
    const eventId = parseInt( this.route.snapshot.paramMap.get('id') );
    console.log('Die Event ID ist ' + eventId );
    this.dataProvider.getEventsByMonth().subscribe((data: any) => {
      data.map( year => {
        year.months.map( month => {
          month.events.map( event => {
            console.log(event.id);
            if (event && event.id === eventId) {
              this.event = event;
            }
          });
        });
      });
    });
  }

  ionViewDidEnter() {
    // console.log(this.event);
    this.defaultHref = `/app/tabs/events`;
  }

  eventClick(item: string) {
    console.log('Clicked', item);
  }

  toggleFavorite() {
    if (this.userProvider.hasFavorite(this.event.name)) {
      this.userProvider.removeFavorite(this.event.name);
      this.isFavorite = false;
    } else {
      this.userProvider.addFavorite(this.event.name);
      this.isFavorite = true;
    }
  }

  shareEvent() {
    // console.log('Clicked share event');
  }

  openNemoWebsiteLink() {
    window.open( 'https://nemo-bb.de/?post_type=events&p=' + this.event.id,'_system', 'location=yes');
  }

  openExternalLink( url ) {
    window.open( url,'_system', 'location=yes');
  }

  writeEmail( email ) {
    window.open( 'mailto:' + email + '?subject=' + this.event.title.rendered );
  }

  getRegisterText() {
    return ( this.isEmail( this.event.acf.email_subscription ) ) ? 'Jetzt per E-Mail anmelden' : 'Jetzt registrieren';
  }

  registerForEvent( data ) {
    if ( this.isEmail( data ) ) {
      return this.writeEmail( data );
    } else {
      return this.openExternalLink( data );
    }
  }

  isEmail( data ) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String( data ).toLowerCase());
  }
}
