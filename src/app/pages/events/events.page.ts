import { Component, OnInit } from '@angular/core';
import { EventData } from '../../providers/event-data';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import {Config} from "@ionic/angular";

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {
  ios: boolean;

  events: any = [];
  eventGroups: any = [];
  segment;
  isLoading = true;

  constructor(
    public eventData: EventData,
    public activatedRoute: ActivatedRoute,
    private config: Config,
    private router: Router
  ) {}

  ngOnInit() {
    this.segment = this.getSegment();
    this.loadEvents();
  }

  ionViewWillEnter() {
    this.ios = this.config.get('mode') === `ios`;

    this.segment = this.getSegment();
    this.loadEvents();
  }

  getSegment() {
    const url = this.router.url;
    switch ( url ) {
      case '/app/tabs/junior':
        return 2;
      case '/app/tabs/mid':
        return 3;
      case '/app/tabs/senior':
        return 4;
      case '/app/tabs/girls':
        return 5;
      default:
        return 'all';
    }
  }

  loadEvents() {
    this.eventData.getEventsByMonth( false, this.segment ).subscribe( (data: any) => {
      this.isLoading = false;
      this.eventGroups = data;
    });
  }

  updateEvents() {
      this.loadEvents();
  }

  countEvents() {
    let amountOfEvents = 0;
    this.eventGroups.map( year => {
      year.months.map( month => {
        month.events.map( ev => {
          if ( !ev.hide ) {
            amountOfEvents++;
          }
        });
      });
    });

    return amountOfEvents;
  }

  hasYearEvents( year ) {

    console.log(year);
    let hasEvents = false;
    year.months.forEach( month => {
      if ( month.events.length > 0 ) {
        hasEvents = true;
      }
    });

    return hasEvents;
  }

  hasMonthEvents( month ) {
    let hasVisibleEvents = false;
    if ( month.events.length > 0 ) {
      month.events.map( event => {
        if ( !event.hide ) {
          hasVisibleEvents = true;
        }
      });
    }
    return hasVisibleEvents;
  }

  getMonthName( month ) {
    switch ( month ) {
      case 0:
        return 'Januar';
      case 1:
        return 'Februar';
      case 2:
        return 'März';
      case 3:
        return 'April';
      case 4:
        return 'Mai';
      case 5:
        return 'Juni';
      case 6:
        return 'Juli';
      case 7:
        return 'August';
      case 8:
        return 'September';
      case 9:
        return 'Oktober';
      case 10:
        return 'November';
      case 11:
        return 'Dezember';
    }
  }
}
