import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  slideOptions = {
    initialSlide: 1,
    speed: 400,
    autoplay:true
  };

  constructor() {

  }

  ngOnInit() {
  }

  openSponsorWebsite( sponsor ) {

    let url = 'https://www.diebank.de/wir-fuer-sie/stiftung-engagement/plattform-fuer-junge-wissenschaftler---nemo.html';
    if ( sponsor === 'sparkasse' ) {
      url = 'https://www.kskbb.de/de/home.html';
    }
    window.open( url, '_system', 'location=yes');
  }
}
