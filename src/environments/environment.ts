// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBZ-M-2X236pIyf8S6GugkjZj0w2GUkvFA",
    authDomain: "nemo-app-76151.firebaseapp.com",
    databaseURL: "https://nemo-app-76151.firebaseio.com",
    projectId: "nemo-app-76151",
    storageBucket: "nemo-app-76151.appspot.com",
    messagingSenderId: "454709761134",
    appId: "1:454709761134:web:0d3d4ccf12891d8db8027f",
    measurementId: "G-3TPL2DHSDY"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
